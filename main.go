package cook_it

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"os"
	"path/filepath"
)

var RepoUrl = os.Getenv("REPO")
var RepoDir, _ = filepath.Abs("./repo")
var RenderedDir, _ = filepath.Abs("./static/rendered")
var TmpDir, _ = filepath.Abs("./tmp")
var ImgDir, _ = filepath.Abs("./static/images")

var Cookbook []Category
var info = Info{
	Title: os.Getenv("TITLE"),
	RepoURL: RepoUrl,
	Icon: os.Getenv("ICON"),
	Headline: os.Getenv("HEADLINE"),
}

func Start() {
	update()

	fmt.Printf("[info] Starting Server on Port 9999\n")
	router := gin.Default()
	router.StaticFile("/static/bulma.min.css", "./static/bulma.min.css")
	router.Static("/static/fonts/", "./static/fonts/")
	router.Static("/static/images/", ImgDir)
	router.GET("/", getFile)
	router.GET("/r/:name", getFile)
	router.POST("/api/update", webhook)
	router.Run(":9999")
}