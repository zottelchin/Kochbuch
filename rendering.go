package cook_it

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var t *template.Template

func update() {
	os.RemoveAll(TmpDir)
	cloneRepo()
	renderAll()
	renderIndex()
	copyImages()
	os.RemoveAll(RepoDir)
	os.RemoveAll(RenderedDir)
	err := os.Rename(TmpDir, RenderedDir)
	if err !=  nil {
		fmt.Printf("Rename failed: %s\n", err.Error())
	}
	os.RemoveAll(TmpDir)
}

func renderAll() {
	root := false
	os.Mkdir(TmpDir, 0777)
	files, _ := ioutil.ReadDir(RepoDir)
	for _, file :=  range files {
		if file.Name() == ".git" {
			continue
		}
		if file.IsDir() && strings.ToLower(file.Name()) != "images" {
			renderSingleDir(filepath.Join(RepoDir, file.Name()), file.Name())
		} else if !root {
			renderSingleDir(RepoDir, "Unsortiert")
			root = true
		}
	}
}

func renderSingleDir(dir, cat string) {
	c := Category{Name: cat}
	files, _ := ioutil.ReadDir(dir)
	for _, file := range files {
		if file.IsDir() || !strings.HasSuffix(file.Name(), ".md") { continue }

		r := parseFile(filepath.Join(dir, file.Name()), cat)
		f, err := os.Create(filepath.Join(TmpDir, r.Uri + ".html"))
		if err != nil {
			fmt.Printf("Failed to create File! %s\n", err.Error())
			continue
		}
		c.Recipes = append(c.Recipes, r)
		t.ExecuteTemplate(f, "one", map[string]interface{}{
			"recipe": r,
			"global": info,
		})
		f.Close()
	}
	Cookbook = append(Cookbook, c)
}

func renderIndex() {
	f, err := os.Create(filepath.Join(TmpDir, "index.html"))
	if err != nil {
		fmt.Printf("Failed to create File! %s\n", err.Error())
		return
	}
	t.ExecuteTemplate(f, "overview", map[string]interface{}{
		"cookbook": Cookbook,
		"global":  info,
	})
	f.Close()
	Cookbook = nil
}

func copyImages() {
	path := filepath.Join(RepoDir, "images")
	if _, err := os.Stat(path); os.IsNotExist(err) {
		fmt.Printf("[Info] No Imagesfolder in Repo\n")
		return
	}
	os.RemoveAll(ImgDir)
	os.Rename(path, ImgDir)
	fmt.Printf("[Info] Imagesfolder copied\n")
}

func init() {
	var err error
	t, err = template.ParseGlob("./templates/*.tmpl")
	if err != nil {
		fmt.Printf("Failed to Parse Templates: %s", err.Error())
	}

	line := ""
	for _, tp := range t.Templates() {
		line += "\t- " + tp.Name() + "\n"
	}
	fmt.Printf("Loaded HTML Templates (%d):\n%s", len(t.Templates()), line)
}