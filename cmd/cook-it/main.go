package main

import (
	cook_it "cook-it"
	"fmt"
	"os"
)

func main() {
	requireEnv("REPO")
	requireEnv("SECRET")
	cook_it.Start()
}

func requireEnv(env string) {
	if _, ok := os.LookupEnv(env); !ok {
		fmt.Printf("[Error] Umgebungsvariable %s nicht gesetzt!\n", env)
		os.Exit(3)
	}

}