package cook_it

import (
	"fmt"
	"github.com/go-git/go-git/v5"
	"os"
	"strings"
)

func cloneRepo() {
	os.RemoveAll(RepoDir)
	r, err := git.PlainClone(RepoDir, false, &git.CloneOptions{
		URL:      RepoUrl,
		Progress: nil,
		Depth:    1,
	})

	if err != nil {
		fmt.Printf("[ERROR] Failed to clone REPO: %s\n", err.Error())
		return
	}

	fmt.Printf("[Info] Cloned into %s. Commit is >%s<\n", RepoDir, headToMessage(r))
}

func headToMessage(r *git.Repository) string {
	ref, _ := r.Head()
	commit, err := r.CommitObject(ref.Hash())
	if err != nil {
		fmt.Printf("[ERROR] Failed to get CommitObject: %s\n", err.Error())
		return ""
	}
	return strings.TrimSpace(strings.Split(commit.Message, "\n")[0])
}