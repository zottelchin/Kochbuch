package cook_it

import (
	"bufio"
	"fmt"
	"github.com/russross/blackfriday/v2"
	"html/template"
	"os"
	"strings"
)

func parseFile(filepath string, cat string) Recipe {
	fmt.Printf("[Info] Parsing %s ...\n", filepath)
	file, err := os.Open(filepath)
	if err != nil {
		return Recipe{}
	}
	defer file.Close()

	recipe := Recipe{
		Metas: make(map[string]string),
		Ingredients: make(map[string]string),
		Category: cat,
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "# ") {
			recipe.Name = strings.TrimPrefix(line, "# ")
			recipe.Uri = strings.ReplaceAll(recipe.Name, " ", "_")
			continue
		}
		if strings.Contains(line, "## Zutaten") {
			for scanner.Scan() {
				line = scanner.Text()
				if strings.HasPrefix(line, "#") {
					break
				}
				parts := strings.Split(line, ":")
				if len(parts) == 2 {
					recipe.Ingredients[strings.TrimSpace(parts[0])] = strings.TrimSpace(parts[1])
				}
			}
		}
		if strings.Contains(line, "## Zubereitung") {
			for scanner.Scan() {
				line = scanner.Text()
				if strings.HasPrefix(line, "#") {
					break
				}
				parts := strings.SplitN(line, ".", 2)
				if len(parts) == 2 {
					recipe.Steps = append(recipe.Steps, template.HTML(blackfriday.Run([]byte(strings.TrimSpace(parts[1])))))
				}
			}
			continue
		}
		if !strings.HasPrefix(line, "#") && strings.Contains(line, ":"){
			parts := strings.SplitN(line, ":", 2)
			recipe.Metas[strings.TrimSpace(parts[0])] = strings.TrimSpace(parts[1])
		}
	}
	//fmt.Printf("%+v\n", recipe)
	return recipe
}
