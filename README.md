# Kochbuch

Hoste dein Kochbuch einfach selber aus einem Git Repo.

Beispiel: [Phillipps Rezepte][Beispiel]

Das Repo mit den Rezepten dahinter findet du [hier][Rezepte].

## Envs:
Key | erforderlich | Beschreibung
----|--------------|-------------
SECRET| ja | geheimer Schlüssel, der im Webhook enthalten sein muss
REPO| ja | Link zum GIT Repo mit den Rezepten
TITLE | nein | Website Titel (Tab Title)
ICON | nein | Icon vor der Headline (zukünftig auch FavIcon) name der remixicon-Klasse ohne 'ri-'
HEADLINE | nein | Überschrift auf der Übersichtsseite

[Rezepte]: https://codeberg.org/zottelchin/Rezepte
[Beispiel]: https://rezepte.zottelchin.de

## Instalation

Per Docker Compose:
```
version: "3"

services:
    app:
        image: zottelchin/cook-it
        restart: unless-stopped
        environment:
            - REPO=https://codeberg.org/...
            - SECRET=change_me
        ports: [80:9999]
```

Per Docker:
`docker run -d -e REPO=https://codeberg.org/... -e SECRET=change_me -p 80:9999 zottelchin/cook-it`

## Webhook einrichten:
Damit die Rezepte auch aktuallisiert werden, wenn sich das Repository ändert, kann man einen Webhook zu diesem hinzufügen.
Dieser muss auf $Website-URL/api/update zeigen, per POST Request gesendet werden und im JSON Body unter dem Key `secret` mit dem Wert der Umgebunsvariable SECRET übereinstimen.

Bei mir sieht das zum Beispiel so aus:
![Webhook Screenshot](img.png)