package cook_it

import (
	"github.com/gin-gonic/gin"
	"os"
	"path/filepath"
	"strings"
)

func getFile (c *gin.Context) {
	name := c.Param("name")
	if name == "" {
		name = "index"
	}
	name = strings.ReplaceAll(name, " ", "_") + ".html"
	file := filepath.Join(RenderedDir, name)

	if _, err := os.Stat(file); err == nil {
		c.File(file)
		return
	}
	c.Redirect(404, "/")
}

func webhook(c *gin.Context){
	hookBody := Webhook{}
	c.BindJSON(&hookBody)
	if hookBody.Secret == os.Getenv("SECRET") {
		update()
		c.Status(200)
		return
	}
	c.Status(401)
}