package cook_it

import "html/template"

type Recipe struct {
	Name string
	Steps []template.HTML
	Ingredients map[string]string
	Metas map[string]string
	Category string
	Uri string
}

type Webhook struct {
	Secret string `json:"secret"`
}

type Category struct {
	Name string
	Recipes []Recipe
}

type Info struct {
	RepoURL string
	Title string
	Icon string
	Headline string
}