FROM golang:alpine as build
WORKDIR /app
ADD cmd/cook-it/main.go cmd/cook-it/main.go
ADD go.mod .
ADD *.go ./
RUN apk --no-cache add upx
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -ldflags="-w -s" -o cook-it cmd/cook-it/main.go
RUN upx cook-it

FROM alpine:latest as network
RUN apk --no-cache add tzdata zip ca-certificates
WORKDIR /usr/share/zoneinfo
RUN zip -r -0 /zoneinfo.zip .

FROM scratch
WORKDIR /
COPY --from=build /app/cook-it .
ADD templates/ ./templates/
ADD static/ ./static/
COPY --from=network /zoneinfo.zip /
COPY --from=network /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENV GIN_MODE=release
EXPOSE 9999
ENTRYPOINT ["/cook-it"]

